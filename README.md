To learn how to install this feature, follow the [Installing Drupal 8 Features](https://designsystem.wwu.edu/installing-drupal-8-features) guide.

A quick search custom block can be used to filter an unordered list based on input.

This block must be paired with an unordered list with the following wrapper and markup classes:

    <div class="quick-search-container">
      <ul class="quick-search-list">
        <li>Searchable item</li>
        <li>Searchable item</li>
      </ul>
    </div>

Typically, this list will be created through views. The classes can be added through views configuration. If the list is small/simple enough, you can insert it directly in the HTML.

It is recommended to include a heading called "Search Results" before the list for ease of navigation to the search results. If you don't want the heading visually displayed, you can still provide the heading while hiding it visually with the following markup:

    <h3 class="visually-hidden">Search Results</h3>

This will allow users that need the heading to navigate to the results easily.